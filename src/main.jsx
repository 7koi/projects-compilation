import { render } from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import App from "./App";
import Error404 from "./routes/Error404";
import C0D3_BC from "./routes/C0D3_BC";
import FE_Mentor from "./routes/FE_Mentor";
import Scrimba from "./routes/Scrimba";
import Leon_BC from "./routes/Leon_BC";

const rootElement = document.getElementById("root");
render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App />} />
      <Route path="/c0d3-bc" element={<C0D3_BC />} />
      <Route path="/front-end-mentor" element={<FE_Mentor />} />
      <Route path="/leon-bc" element={<Leon_BC />} />
      <Route path="/scrimba" element={<Scrimba />} />
      <Route path="*" element={<Error404 />} />
    </Routes>
  </BrowserRouter>,
  rootElement
);
