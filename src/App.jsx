import { Outlet, Link } from "react-router-dom";

export default function App() {
  return (
    <div>
      <nav>
        <ul className="navbar">
          <il>
            <Link to="/">LOGO</Link>
          </il>
          <il>
            <Link to="/leon-bc">Leon BC</Link>
          </il>
          <il>
            <Link to="/c0d3-bc">C0D3 BC</Link>
          </il>
          <il>
            <Link to="/scrimba">Scrimba</Link>
          </il>
          <il>
            <Link to="/front-end-mentor">Front-End Mentor</Link>
          </il>
        </ul>
      </nav>
      <h1>This is my main page</h1>
    </div>
  );
}
